/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
  private readonly users = [
    {
      id: 1,
      email: 'john@email.com',
      password: 'changeme1234',
    },
    {
      id: 2,
      email: 'maria@email.com',
      password: 'guess1234',
    },
  ];

  async findOne(email: string): Promise<User | undefined> {
    return this.users.find((user) => user.email === email);
  }
}
